package com.ar.superdigital.gylgroup.transport;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CardsAvaiable extends AppCompatActivity {

    ArrayList<ObjCard> CardsAvaiable;
    ObjCard objCard;
    ListView lv_cards;
    adpCards adpCards;
    Button ib_vtb;
    int antCard=-1;
    int click=0;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards_avaiable);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        pDialog = new ProgressDialog(CardsAvaiable.this);
        pDialog.setMessage("Espere un Momento");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);

        CardsAvaiable=new ArrayList<ObjCard>();
        lv_cards = (ListView) findViewById(R.id.lv_cards);
        ib_vtb=findViewById(R.id.ib_vtb);

        redraw();

        lv_cards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                selectCard(position);
                System.out.println("position:------------------------->"+position);
            }
        });

        ib_vtb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), cardLink.class);
                startActivityForResult(intent, 2);
            }
        });

    }

    private void redraw() {
        if(Globals.getCardsAvaiable() != null) {
            CardsAvaiable= Globals.getCardsAvaiable();
        }

        adpCards = new adpCards(CardsAvaiable.this, CardsAvaiable);
        lv_cards.setAdapter(adpCards);

        if(CardsAvaiable.size() >= Globals.getMaxCountCards()){
            ib_vtb.setVisibility(View.INVISIBLE);
        }

        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) lv_cards.getLayoutParams();
        layoutParams.height = (CardsAvaiable.size() * 215) + 500;
        lv_cards.setLayoutParams(layoutParams);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        System.out.println("requestCode:--------------------------->"+requestCode);
        System.out.println("resultCode:--------------------------->"+resultCode);
        if (requestCode == 1){
            redraw();
        }
        if ((requestCode == 2) && (resultCode == RESULT_OK)){
            relectura();
        }
    }

    private void selectCard(int position) {

        click = click+1;

        System.out.println("antCard:----------------->"+antCard);
        System.out.println("card:----------------->"+position);
        System.out.println("click:----------------->"+click);

        if(position==antCard && click==2) {
            Globals.setCardSelect(position);
            Intent intent = new Intent(getApplicationContext(), card_detail.class);
            startActivityForResult(intent, 1);
        } else {
            click =1;
        }

        antCard=position;

        colpaseCards();
        CardsAvaiable.get(position).setHeight(Globals.getCardHeight());
        adpCards.notifyDataSetChanged();

    }

    private void colpaseCards() {

        for(int i=0; i<CardsAvaiable.size(); i++){
            CardsAvaiable.get(i).setHeight(Globals.getColapsedCardHeight());
            adpCards.notifyDataSetChanged();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cerrarapp) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void relectura() {

        pDialog.show();

        System.out.println("entrando al cardsAvailable --------------------------->");

        String tag_json_obj = "json_obj_req";
        String url = Globals.getUrlService()+"/accounts/"+Globals.getAccountNumber()+"/linked-cards";

        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String tmpTxt=response.toString();
                        System.out.println("archivo----------------------------------->"+tmpTxt);

                        try {

                            JSONObject jsonObject=new JSONObject(tmpTxt);
                            JSONArray jsonArray=jsonObject.getJSONArray("result");

                            System.out.println("Elements:-------------------->"+jsonArray.length());

                            if(jsonArray.length() > 0) {

                                CardsAvaiable=new ArrayList<ObjCard>();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    objCard = new ObjCard();
                                    objCard.setTransportCardId(Long.parseLong(jsonObject1.getString("transportCardId")));
                                    objCard.setAccountId(Long.parseLong(jsonObject1.getString("accountId")));
                                    objCard.setCardNumber(Long.parseLong(jsonObject1.getString("cardNumber")));
                                    objCard.setCardTypeId(Integer.parseInt(jsonObject1.getString("cardTypeId")));
                                    objCard.setCardStatusId(Integer.parseInt(jsonObject1.getString("cardStatusId")));
                                    objCard.setCardAliasName(jsonObject1.getString("cardAliasName"));
                                    objCard.setAutomaticRecharge(Integer.parseInt(jsonObject1.getString("automaticRecharge")));
                                    objCard.setMainCard(Integer.parseInt(jsonObject1.getString("mainCard")));
                                    objCard.setTypeDescription(jsonObject1.getString("typeDescription"));
                                    objCard.setStatusDescription(jsonObject1.getString("statusDescription"));
                                    objCard.setMinBalanceRecharge(Double.parseDouble(jsonObject1.getString("minBalanceRecharge")));
                                    objCard.setRechargeAmount(Double.parseDouble(jsonObject1.getString("rechargeAmount")));
                                    objCard.setRechargeAmountAutomatic(Double.parseDouble(jsonObject1.getString("rechargeAmount")));
                                    objCard.setHeight(Globals.getColapsedCardHeight());

                                    CardsAvaiable.add(objCard);

                                    System.out.println("Numero de tarjeta ---------------------------->" + jsonObject1.getString("cardNumber"));

                                }
                                CardsAvaiable.get(CardsAvaiable.size() - 1).setHeight(Globals.getCardHeight());
                                Globals.setCardsAvaiable(CardsAvaiable);
                                redraw();
                                pDialog.dismiss();
                            }


                        } catch(JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERROR-------------------->"+error.getMessage());

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("------------------------>"+json);
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                    if(networkResponse.statusCode==500) {
                        relectura();
                    }
                }
            }
        });

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

}
