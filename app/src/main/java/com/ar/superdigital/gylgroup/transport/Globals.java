package com.ar.superdigital.gylgroup.transport;

import java.util.ArrayList;

public class Globals {

    private static int maxCountCards=5;
    private static ArrayList<ObjCard> CardsAvaiable;
    private static int colapsedCardHeight=210;
    private static int cardHeight=600;
    private static int cardSelect=0;

    private static String accountNumber;
    private static double montoUserMain;
    private static String urlService="https://api.dev.gruposuperdigital.com/bcc-transport";

    public Globals() {

    }

    public Globals(int maxCountCards) {
        this.maxCountCards = maxCountCards;
    }

    public static double getMontoUserMain() {
        return montoUserMain;
    }

    public static void setMontoUserMain(double montoUserMain) {
        Globals.montoUserMain = montoUserMain;
    }

    public static String getAccountNumber() {
        return accountNumber;
    }

    public static void setAccountNumber(String accountNumber) {
        Globals.accountNumber = accountNumber;
    }

    public static String getUrlService() {
        return urlService;
    }

    public static void setUrlService(String urlService) {
        Globals.urlService = urlService;
    }

    public static int getMaxCountCards() {
        return maxCountCards;
    }

    public static ArrayList<ObjCard> getCardsAvaiable() {
        return CardsAvaiable;
    }

    public static void setCardsAvaiable(ArrayList<ObjCard> cardsAvaiable) {
        CardsAvaiable = cardsAvaiable;
    }

    public static int getColapsedCardHeight() {
        return colapsedCardHeight;
    }

    public static void setColapsedCardHeight(int colapsedCardHeight) {
        Globals.colapsedCardHeight = colapsedCardHeight;
    }

    public static int getCardHeight() {
        return cardHeight;
    }

    public static void setCardHeight(int cardHeight) {
        Globals.cardHeight = cardHeight;
    }

    public static int getCardSelect() {
        return cardSelect;
    }

    public static void setCardSelect(int cardSelect) {
        Globals.cardSelect = cardSelect;
    }
}



