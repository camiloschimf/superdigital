package com.ar.superdigital.gylgroup.transport;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InitBasicApp extends AppCompatActivity {

    Button ib_vtb;
    EditText etNombreUser;
    EditText etMontoUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_basic_app);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        etNombreUser=findViewById(R.id.editText2);
        etMontoUser=findViewById(R.id.editText3);

        ib_vtb=findViewById(R.id.ib_vtb);
        ib_vtb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ib_vtb.getText().toString() != "") {
                    Globals.setAccountNumber(etNombreUser.getText().toString());
                    Globals.setMontoUserMain(Double.valueOf(etMontoUser.getText().toString()));
                    System.out.println("CN:------------------>"+Globals.getAccountNumber());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
            }

        });


    }



}
