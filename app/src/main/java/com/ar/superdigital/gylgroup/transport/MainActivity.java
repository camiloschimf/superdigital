package com.ar.superdigital.gylgroup.transport;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    static ObjCard objCard;
    static Button ib_vincularTarjeta;
    static ArrayList<ObjCard> arrCards;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Espere un Momento");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);

        ib_vincularTarjeta=findViewById(R.id.ib_vtb);
        ib_vincularTarjeta.setVisibility(View.INVISIBLE);
        ib_vincularTarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Globals.getCardsAvaiable() != null) {
                    Intent intent = new Intent(getApplicationContext(), CardsAvaiable.class);
                    startActivityForResult(intent, 1);
                } else {
                    Intent intent = new Intent(getApplicationContext(), cardLink.class);
                    startActivityForResult(intent, 2);
                }
            }
        });

        if(isOnline()) {
            System.out.println("estamos on line----------------------------->");
            cardsAvailable();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        System.out.println("requestCode:--------------------------->"+requestCode);
        System.out.println("resultCode:--------------------------->"+resultCode);
        if ((requestCode == 2) && (resultCode == RESULT_OK)){
            relectura();
        }
        if ((requestCode == 1 || requestCode == 2) && (resultCode == RESULT_CANCELED)){
            ib_vincularTarjeta.setVisibility(View.VISIBLE);
        }
    }


    private void relectura() {

        pDialog.show();

        System.out.println("entrando al relectura --------------------------->");

        String tag_json_obj = "json_obj_req";
        String url = Globals.getUrlService()+"/accounts/"+Globals.getAccountNumber()+"/linked-cards";

        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String tmpTxt=response.toString();
                        System.out.println("archivo----------------------------------->"+tmpTxt);

                        try {

                            JSONObject jsonObject=new JSONObject(tmpTxt);
                            JSONArray jsonArray=jsonObject.getJSONArray("result");

                            System.out.println("Elements:-------------------->"+jsonArray.length());

                            if(jsonArray.length() > 0) {

                                arrCards=new ArrayList<ObjCard>();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    objCard = new ObjCard();
                                    objCard.setTransportCardId(Long.parseLong(jsonObject1.getString("transportCardId")));
                                    objCard.setAccountId(Long.parseLong(jsonObject1.getString("accountId")));
                                    objCard.setCardNumber(Long.parseLong(jsonObject1.getString("cardNumber")));
                                    objCard.setCardTypeId(Integer.parseInt(jsonObject1.getString("cardTypeId")));
                                    objCard.setCardStatusId(Integer.parseInt(jsonObject1.getString("cardStatusId")));
                                    objCard.setCardAliasName(jsonObject1.getString("cardAliasName"));
                                    objCard.setAutomaticRecharge(Integer.parseInt(jsonObject1.getString("automaticRecharge")));
                                    objCard.setMainCard(Integer.parseInt(jsonObject1.getString("mainCard")));
                                    objCard.setTypeDescription(jsonObject1.getString("typeDescription"));
                                    objCard.setStatusDescription(jsonObject1.getString("statusDescription"));
                                    objCard.setMinBalanceRecharge(Double.parseDouble(jsonObject1.getString("minBalanceRecharge")));
                                    objCard.setRechargeAmount(Double.parseDouble(jsonObject1.getString("rechargeAmount")));
                                    objCard.setRechargeAmountAutomatic(Double.parseDouble(jsonObject1.getString("rechargeAmount")));
                                    objCard.setHeight(Globals.getColapsedCardHeight());
                                    arrCards.add(objCard);

                                    System.out.println("Numero de tarjeta ---------------------------->" + jsonObject1.getString("cardNumber"));

                                }
                                arrCards.get(arrCards.size() - 1).setHeight(Globals.getCardHeight());
                                Globals.setCardsAvaiable(arrCards);
                                Intent intent = new Intent(getApplicationContext(), CardsAvaiable.class);
                                startActivityForResult(intent, 1);
                                pDialog.dismiss();
                            }


                        } catch(JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERROR-------------------->"+error.getMessage());

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("------------------------>"+json);
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                    if(networkResponse.statusCode==500) {
                        relectura();
                    }
                }
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    public void cardsAvailable(){

        arrCards=new ArrayList<ObjCard>();

        System.out.println("entrando al cardsAvailable --------------------------->");

        String tag_json_obj = "json_obj_req";
        String url = Globals.getUrlService()+"/accounts/"+Globals.getAccountNumber()+"/linked-cards";

        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String tmpTxt=response.toString();
                        System.out.println("archivo----------------------------------->"+tmpTxt);

                        try {

                            JSONObject jsonObject=new JSONObject(tmpTxt);
                            JSONArray jsonArray=jsonObject.getJSONArray("result");

                            System.out.println("Elements:-------------------->"+jsonArray.length());

                            if(jsonArray.length() > 0) {

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    objCard = new ObjCard();
                                    objCard.setTransportCardId(Long.parseLong(jsonObject1.getString("transportCardId")));
                                    objCard.setAccountId(Long.parseLong(jsonObject1.getString("accountId")));
                                    objCard.setCardNumber(Long.parseLong(jsonObject1.getString("cardNumber")));
                                    objCard.setCardTypeId(Integer.parseInt(jsonObject1.getString("cardTypeId")));
                                    objCard.setCardStatusId(Integer.parseInt(jsonObject1.getString("cardStatusId")));
                                    objCard.setCardAliasName(jsonObject1.getString("cardAliasName"));
                                    objCard.setAutomaticRecharge(Integer.parseInt(jsonObject1.getString("automaticRecharge")));
                                    objCard.setMainCard(Integer.parseInt(jsonObject1.getString("mainCard")));
                                    objCard.setTypeDescription(jsonObject1.getString("typeDescription"));
                                    objCard.setStatusDescription(jsonObject1.getString("statusDescription"));
                                    objCard.setMinBalanceRecharge(Double.parseDouble(jsonObject1.getString("minBalanceRecharge")));
                                    objCard.setRechargeAmount(Double.parseDouble(jsonObject1.getString("rechargeAmount")));
                                    objCard.setRechargeAmountAutomatic(Double.parseDouble(jsonObject1.getString("rechargeAmount")));
                                    objCard.setHeight(Globals.getColapsedCardHeight());

                                    arrCards.add(objCard);
                                    System.out.println("Numero de tarjeta ---------------------------->" + jsonObject1.getString("cardNumber"));

                                }
                                arrCards.get(arrCards.size() - 1).setHeight(Globals.getCardHeight());
                                Globals.setCardsAvaiable(arrCards);
                            }

                            activateButton();

                        } catch(JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERROR-------------------->"+error.getMessage());

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("------------------------>"+json);
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                    if(networkResponse.statusCode==500) {
                        cardsAvailable();
                    }
                }
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    private void activateButton() {
        if(arrCards.size() > 0) {
            ib_vincularTarjeta.setText("VER TARJETAS");
            Intent intent = new Intent(getApplicationContext(), CardsAvaiable.class);
            startActivityForResult(intent, 1);
        } else {
            ib_vincularTarjeta.setText("VINCULAR TARJETA");
            //Intent intent = new Intent(getApplicationContext(), cardLink.class);
            //startActivityForResult(intent, 2);
            ib_vincularTarjeta.setVisibility(View.VISIBLE);
        }
        //ib_vincularTarjeta.setVisibility(View.VISIBLE);
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cerrarapp) {
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





}
