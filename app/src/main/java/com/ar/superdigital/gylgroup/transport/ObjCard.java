package com.ar.superdigital.gylgroup.transport;

public class ObjCard {

    long transportCardId;
    long accountId;
    long cardNumber;
    int cardTypeId;
    int cardStatusId;
    String cardAliasName;
    int automaticRecharge;
    double rechargeAmountAutomatic;
    int mainCard;
    String typeDescription;
    String statusDescription;
    double minBalanceRecharge;
    double rechargeAmount;

    double balance;
    String balanceDate;


    private int height;



    public ObjCard() {
    }

    public ObjCard(long transportCardId, long accountId, long cardNumber, int cardTypeId, int cardStatusId, String cardAliasName, int automaticRecharge, int mainCard, String typeDescription, String statusDescription, Double minBalanceRecharge, double rechargeAmount) {
        this.transportCardId = transportCardId;
        this.accountId = accountId;
        this.cardNumber = cardNumber;
        this.cardTypeId = cardTypeId;
        this.cardStatusId = cardStatusId;
        this.cardAliasName = cardAliasName;
        this.automaticRecharge = automaticRecharge;
        this.mainCard = mainCard;
        this.typeDescription = typeDescription;
        this.statusDescription = statusDescription;
        this.minBalanceRecharge = minBalanceRecharge;
        this.rechargeAmount = rechargeAmount;
    }

    public String getBalanceDate() {
        return balanceDate;
    }

    public void setBalanceDate(String balanceDate) {
        this.balanceDate = balanceDate;
    }

    public double getRechargeAmountAutomatic() {
        return rechargeAmountAutomatic;
    }

    public void setRechargeAmountAutomatic(double rechargeAmountAutomatic) {
        this.rechargeAmountAutomatic = rechargeAmountAutomatic;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public long getTransportCardId() {
        return transportCardId;
    }

    public void setTransportCardId(long transportCardId) {
        this.transportCardId = transportCardId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getCardTypeId() {
        return cardTypeId;
    }

    public void setCardTypeId(int cardTypeId) {
        this.cardTypeId = cardTypeId;
    }

    public int getCardStatusId() {
        return cardStatusId;
    }

    public void setCardStatusId(int cardStatusId) {
        this.cardStatusId = cardStatusId;
    }

    public String getCardAliasName() {
        return cardAliasName;
    }

    public void setCardAliasName(String cardAliasName) {
        this.cardAliasName = cardAliasName;
    }

    public int getAutomaticRecharge() {
        return automaticRecharge;
    }

    public void setAutomaticRecharge(int automaticRecharge) {
        this.automaticRecharge = automaticRecharge;
    }

    public int getMainCard() {
        return mainCard;
    }

    public void setMainCard(int mainCard) {
        this.mainCard = mainCard;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public Double getMinBalanceRecharge() {
        return minBalanceRecharge;
    }

    public void setMinBalanceRecharge(Double minBalanceRecharge) {
        this.minBalanceRecharge = minBalanceRecharge;
    }

    public double getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(double rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
