package com.ar.superdigital.gylgroup.transport;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;

public class adpCards extends BaseAdapter {

    protected Activity activity;
    protected ArrayList<ObjCard> items;

    public adpCards(Activity activity, ArrayList<ObjCard> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getTransportCardId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.list_cards, null);
        }

        ObjCard objCard = items.get(position);

        TextView tv_tit_card = (TextView) vi.findViewById(R.id.tv_name_card_1);
        tv_tit_card.setText(objCard.getCardAliasName());

        TextView tv_number_card = (TextView) vi.findViewById(R.id.tv_number_card_1);
        tv_number_card.setText("Nº "+objCard.getCardNumber());

        ImageView img_main_card =  vi.findViewById(R.id.img_main_card_1);
        if(objCard.getMainCard()==1) {
            img_main_card.setVisibility(View.VISIBLE);
        } else {
            img_main_card.setVisibility(View.INVISIBLE);
        }


        CardView cvTmp = vi.findViewById(R.id.cv_card_1);
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) cvTmp.getLayoutParams();
        layoutParams.height = objCard.getHeight();
        cvTmp.setLayoutParams(layoutParams);

        return vi;
    }

}
