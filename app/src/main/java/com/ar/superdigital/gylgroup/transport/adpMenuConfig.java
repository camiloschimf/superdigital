package com.ar.superdigital.gylgroup.transport;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;

public class adpMenuConfig extends BaseAdapter {

    protected Activity activity;
    protected ArrayList<objMenuConfig> items;

    public adpMenuConfig(Activity activity, ArrayList<objMenuConfig> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.list_menu_config, null);
        }

        objMenuConfig objMenuConfig = items.get(position);

        TextView tv_titleMenu = vi.findViewById(R.id.tv_titleMenu);
        tv_titleMenu.setText(objMenuConfig.getTitulo());

        if(objMenuConfig.getTitleColor() != null) {
            TextView cvTmp = vi.findViewById(R.id.tv_titleMenu);
            cvTmp.setTextColor(Color.parseColor(objMenuConfig.getTitleColor()));
        }

        ImageView ivi = vi.findViewById(R.id.imageView2);
        ImageView iva = vi.findViewById(R.id.imageView4);
        if(objMenuConfig.isActivo()) {
            ivi.setVisibility(View.INVISIBLE);
            iva.setVisibility(View.VISIBLE);
        }

        if(!objMenuConfig.isAddActivo()) {
            ivi.setVisibility(View.INVISIBLE);
        }

        return vi;
    }
}
