package com.ar.superdigital.gylgroup.transport;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class cardAutomaticCharge extends AppCompatActivity {

    static ArrayList<ObjCard> CardsAvaiable;
    ObjCard objCard;
    static  String[] rechargueOptions;
    static Spinner spRechargue;
    Spinner spMinChargue;
    Switch swActivoRA;
    Button btSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_automatic_charge);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        CardsAvaiable=new ArrayList<ObjCard>();
        if(Globals.getCardsAvaiable() != null) {
            CardsAvaiable= Globals.getCardsAvaiable();
        }

        objCard = CardsAvaiable.get(Globals.getCardSelect());
        cardData();

        swActivoRA = findViewById(R.id.switch1);
        swActivoRA.setChecked(objCard.getAutomaticRecharge()==1);
        swActivoRA.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    objCard.setAutomaticRecharge(1);
                } else {
                    objCard.setAutomaticRecharge(0);
                }
            }
        });

        spRechargue=findViewById(R.id.spinner2);
        spRechargue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String monto=spRechargue.getItemAtPosition(spRechargue.getSelectedItemPosition()).toString();
                int pos= (int) spRechargue.getSelectedItemId();
                objCard.setRechargeAmountAutomatic(Double.valueOf(monto));
                System.out.println("monto seleccionado---------------->"+monto);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        spMinChargue=findViewById(R.id.spinner);
        spMinChargue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String monto=spMinChargue.getItemAtPosition(spMinChargue.getSelectedItemPosition()).toString();
                int pos= (int) spMinChargue.getSelectedItemId();
                objCard.setMinBalanceRecharge(Double.valueOf(monto));
                System.out.println("monto---------------->"+monto);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });


        rechargueOption();

        btSave=findViewById(R.id.ib_vtb);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAutoRechargue();
            }
        });

        System.out.println("minimo:---------------------->"+objCard.getMinBalanceRecharge());
        double data = objCard.getMinBalanceRecharge();
        int value = (int)data;
        spMinChargue.setSelection(obtenerPosicionItem(spMinChargue, String.valueOf(value)));

    }

    public static int obtenerPosicionItem(Spinner spinner, String fruta) {
        int posicion = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(fruta)) {
                posicion = i;
            }
        }
        return posicion;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cerrarapp) {
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void cardData() {

        TextView tv_tit_card = findViewById(R.id.tv_name_card_1);
        tv_tit_card.setText(objCard.getCardAliasName());

        TextView tv_number_card = findViewById(R.id.tv_number_card_1);
        tv_number_card.setText("Nº "+objCard.getCardNumber());

        ImageView img_main_card =  findViewById(R.id.img_main_card_1);
        if(objCard.getMainCard()==1) {
            img_main_card.setVisibility(View.VISIBLE);
        } else {
            img_main_card.setVisibility(View.INVISIBLE);
        }

    }

    public void rechargueOption(){
        System.out.println("entrando al rechargueOption --------------------------->");

        JSONObject postparams  = new JSONObject();
        try {
            postparams.put("transportCardId", objCard.getTransportCardId());
            postparams.put("accountBalance",100000);
        } catch (JSONException e) {
            System.out.println("ERROR PARAMETER:------------------------>"+e.getMessage());
        }

        String tag_json_obj = "json_obj_req";

        String url = Globals.getUrlService()+"/cards/autorecharge-options";

        System.out.println("URL----------------------->"+url);
        System.out.println("param:---------------->"+postparams);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String tmpTxt=response.toString();
                        System.out.println("archivo----------------------------------->"+tmpTxt);

                        try {

                            JSONObject jsonObject=new JSONObject(tmpTxt);
                            JSONArray jsonArray=jsonObject.getJSONArray("result");

                            System.out.println("enfinnnn-------->"+jsonObject.getInt("total"));

                            rechargueOptions=new String[jsonObject.getInt("total")];

                            System.out.println("Elements:-------------------->"+jsonArray.length());

                            if(jsonArray.length() > 0) {

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    System.out.println("option---------->"+jsonArray.get(i).toString());
                                    if(jsonArray.get(i).toString() != null) {
                                        rechargueOptions[i] = jsonArray.get(i).toString();
                                    }
                                }

                                spRechargue.setAdapter(new ArrayAdapter<String>(cardAutomaticCharge.this, android.R.layout.simple_spinner_dropdown_item, rechargueOptions));
                                spMinChargue.setAdapter(new ArrayAdapter<String>(cardAutomaticCharge.this, android.R.layout.simple_spinner_dropdown_item, rechargueOptions));
                            }

                            double data = objCard.getRechargeAmountAutomatic();
                            int value = (int)data;
                            spRechargue.setSelection(obtenerPosicionItem(spRechargue, String.valueOf(value)));

                            double data2 = objCard.getMinBalanceRecharge();
                            int value2 = (int)data2;
                            spMinChargue.setSelection(obtenerPosicionItem(spMinChargue, String.valueOf(value2)));



                        } catch(JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERROR-------------------->"+error.getMessage());

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("------------------------>"+json);
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                }
            }
        });

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);



    }

    private void saveAutoRechargue() {

        System.out.println("entrando al saveAutoRechargue --------------------------->");

        JSONObject postparams  = new JSONObject();
        try {
            postparams.put("transportCardId", objCard.getTransportCardId());
            postparams.put("minBalanceRecharge", objCard.getMinBalanceRecharge());
            postparams.put("maxBalanceRecharge", 0);
            postparams.put("rechargeAmount", objCard.getRechargeAmountAutomatic());
            postparams.put("automaticRecharge", objCard.getAutomaticRecharge());
        } catch (JSONException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("parameter--------------------->"+postparams.toString());

        String tag_json_obj = "json_obj_req";

        String url = Globals.getUrlService()+"/cards/"+objCard.getTransportCardId()+"/autorecharge";

        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PATCH, url, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("Mensaje salida------------------------>"+json);
                    Toast.makeText(cardAutomaticCharge.this,  json, Toast.LENGTH_SHORT).show();
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                }
                System.out.println("ERROR VolleyError------------------->"+error.getMessage());
            }
        }){

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                if(mStatusCode==201 || mStatusCode==200){
                    System.out.println("tarjeta seleccionada:------------------------->"+Globals.getCardSelect());
                    System.out.println("minselect ---------------->"+objCard.getRechargeAmountAutomatic());
                    CardsAvaiable.set(Globals.getCardSelect(), objCard);
                    Globals.setCardsAvaiable(CardsAvaiable);
                    System.out.println("min Guardado------->"+CardsAvaiable.get(Globals.getCardSelect()).getRechargeAmountAutomatic());
                    setResult(RESULT_OK);
                    finish();
                }
                System.out.println(String.valueOf("ECS------------------->"+mStatusCode));
                return super.parseNetworkResponse(response);
            }
        };
        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

}
