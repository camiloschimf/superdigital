package com.ar.superdigital.gylgroup.transport;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class cardData {

    static ObjCard objCard;
    static ArrayList<ObjCard> arrCards;

    public static void cardsAvailable(){

        arrCards=new ArrayList<ObjCard>();
        if(Globals.getCardsAvaiable() != null) {
            arrCards= Globals.getCardsAvaiable();
        }

        System.out.println("entrando al cardsAvailable --------------------------->");

        Map<String, String> params = new HashMap<String, String>();
        params.put("i","k");
        JSONObject postparams  = new JSONObject(params);

        String tag_json_obj = "json_obj_req";
        String url = "https://api.dev.gruposuperdigital.com/bcc-transport/accounts/30123444/linked-cards";

        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String tmpTxt=response.toString();
                        System.out.println("archivo----------------------------------->"+tmpTxt);

                        try {

                            JSONObject jsonObject=new JSONObject(tmpTxt);
                            JSONArray jsonArray=jsonObject.getJSONArray("result");

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);

                                objCard= new ObjCard();
                                objCard.setTransportCardId(Long.parseLong(jsonObject1.getString("transportCardId")));
                                objCard.setAccountId(Long.parseLong(jsonObject1.getString("accountId")));
                                objCard.setCardNumber(Long.parseLong(jsonObject1.getString("cardNumber")));
                                objCard.setCardTypeId(Integer.parseInt(jsonObject1.getString("cardTypeId")));
                                objCard.setCardStatusId(Integer.parseInt(jsonObject1.getString("cardStatusId")));
                                objCard.setCardAliasName(jsonObject1.getString("cardAliasName"));
                                objCard.setAutomaticRecharge(Integer.parseInt(jsonObject1.getString("automaticRecharge")));
                                objCard.setMainCard(Integer.parseInt(jsonObject1.getString("mainCard")));
                                objCard.setTypeDescription(jsonObject1.getString("typeDescription"));
                                objCard.setStatusDescription(jsonObject1.getString("statusDescription"));
                                objCard.setMinBalanceRecharge(Double.parseDouble(jsonObject1.getString("minBalanceRecharge")));
                                objCard.setRechargeAmount(Double.parseDouble(jsonObject1.getString("rechargeAmount")));

                                arrCards.add(objCard);

                                System.out.println("Numero de tarjeta ---------------------------->"+jsonObject1.getString("cardNumber"));

                            }


                        } catch(JSONException e) {
                            e.printStackTrace();
                        }

                        Globals.setCardsAvaiable(arrCards);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERROR-------------------->"+error.getMessage());
            }
        });

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }


}
