package com.ar.superdigital.gylgroup.transport;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class cardLink extends AppCompatActivity {

    Button ib_vinculartarjeta;
    static EditText et_numTarj;
    static EditText et_aliasTarj;
    ArrayList<ObjCard> CardsAvaiable;
    ObjCard ObjCard;
    static CheckBox ch_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_link);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        et_numTarj = findViewById(R.id.et_numTarj);
        et_aliasTarj = findViewById(R.id.et_aliasTarj);

        CardsAvaiable=new ArrayList<ObjCard>();
        if(Globals.getCardsAvaiable() != null) {
            CardsAvaiable= Globals.getCardsAvaiable();
        }

        ObjCard=new ObjCard();

        ch_main=findViewById(R.id.ch_main);
        if(CardsAvaiable.size() == 0) {
            ch_main.setVisibility(View.INVISIBLE);
        }

        ib_vinculartarjeta=findViewById(R.id.ib_vinculartarjeta);
        ib_vinculartarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCard();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cerrarapp) {
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void colpaseCards() {

        for(int i=0; i<CardsAvaiable.size(); i++){
            CardsAvaiable.get(i).setHeight(Globals.getColapsedCardHeight());
        }

    }

    private void addCard() {

        int t=0;
        int a=0;

        System.out.println("Alias:----------------------------->"+et_aliasTarj.getText().toString());
        System.out.println("Numero:----------------------------->"+et_numTarj.getText().toString());

        if(et_aliasTarj.getText().toString().equals("")){
            Toast.makeText(cardLink.this,  "Debe ingresar el Alias de la Tarjeta.", Toast.LENGTH_SHORT).show();
            t=1;
        } else {
            t=0;
        }
        if(et_numTarj.getText().toString().equals("")){
            Toast.makeText(cardLink.this,  "Debe ingresar el Numero de la Tarjeta.", Toast.LENGTH_SHORT).show();
            a=1;
        } else {
            a=0;
        }

        if(t==0 && a==0) {

            try {
                if (CardsAvaiable.size() >= 5) {
                    Toast.makeText(cardLink.this, "No se puede agregar mas tarjetas.", Toast.LENGTH_SHORT).show();
                } else {
                    int ch=0;
                    if(ch_main.isChecked()){
                        desbloquearMain();
                        ch=1;
                    }
                    if(CardsAvaiable.size() == 0){
                        ch=1;
                    }
                    colpaseCards();
                    ObjCard.setCardNumber(Long.parseLong(et_numTarj.getText().toString()));
                    ObjCard.setCardAliasName(et_aliasTarj.getText().toString());
                    ObjCard.setMainCard(ch);
                    ObjCard.setHeight(Globals.getCardHeight());
                    cardAddLink();

                    et_aliasTarj.setText("");
                    et_numTarj.setText("");
                }


            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("---------------------------------------------------------");
                System.out.println(e.getMessage());
                System.out.println("---------------------------------------------------------");
            }

        }

    }

    private void desbloquearMain() {
        for(int i=0; i < CardsAvaiable.size(); i++) {
            CardsAvaiable.get(i).setMainCard(0);
        }
    }


    public void cardAddLink(){

        System.out.println("entrando al cardAddLink --------------------------->");

        JSONObject postparams  = new JSONObject();

        try {
            postparams.put("accountId",Integer.valueOf(Globals.getAccountNumber()));
            postparams.put("cardNumber",ObjCard.getCardNumber());
            postparams.put("cardTypeId",1);
            postparams.put("cardAliasName", ObjCard.getCardAliasName());
            postparams.put("mainCard",ObjCard.getMainCard());
            postparams.put("createdUser",Integer.valueOf(Globals.getAccountNumber()));
            postparams.put("cardStatusId",1);
        } catch (JSONException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("parameter--------------------->"+postparams.toString());

        String tag_json_obj = "json_obj_req";

        String url = Globals.getUrlService()+"/cards/";

        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("Mensaje salida------------------------>"+json);
                    Toast.makeText(cardLink.this,  json, Toast.LENGTH_SHORT).show();
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                }
                System.out.println("ERROR VolleyError------------------->"+error.getMessage());
            }
        }){

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                if(mStatusCode==201 || mStatusCode==200){
                    CardsAvaiable.add(ObjCard);
                    Globals.setCardsAvaiable(CardsAvaiable);
                    setResult(RESULT_OK);
                    finish();
                }
                System.out.println(String.valueOf("ECS------------------->"+mStatusCode));
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }


}
