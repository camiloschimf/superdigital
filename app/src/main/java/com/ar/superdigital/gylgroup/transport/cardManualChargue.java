package com.ar.superdigital.gylgroup.transport;

import android.os.Bundle;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class cardManualChargue extends AppCompatActivity {

    ArrayList<ObjCard> CardsAvaiable;
    ObjCard objCard;
    TextView saldoSuperdigital;
    TextView saldoBip;
    Button btSave;
    TextView tvTextRecMax;
    static card_detail CD;

    static  String[] rechargueOptions;
    static Spinner spRechargue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_manual_chargue);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        tvTextRecMax=findViewById(R.id.textView6);
        tvTextRecMax.setVisibility(View.INVISIBLE);

        CardsAvaiable=new ArrayList<ObjCard>();
        if(Globals.getCardsAvaiable() != null) {
            CardsAvaiable= Globals.getCardsAvaiable();
        }
        objCard = CardsAvaiable.get(Globals.getCardSelect());
        cardData();

        saldoSuperdigital=findViewById(R.id.textView7_2);
        saldoSuperdigital.setText(String.format("%.3f", Globals.getMontoUserMain()));

        saldoBip=findViewById(R.id.textView8_2);
        saldoBip.setText(String.format("%.3f",objCard.getBalance()));

        spRechargue=findViewById(R.id.spinner);
        spRechargue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String monto=spRechargue.getItemAtPosition(spRechargue.getSelectedItemPosition()).toString();
                int pos= (int) spRechargue.getSelectedItemId();
                objCard.setRechargeAmount(Double.valueOf(monto));
                System.out.println("monto seleccionado---------------->"+monto);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        btSave=findViewById(R.id.ib_vtb);
        btSave.setEnabled(false);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveRechargue();
            }
        });

        rechargueOption();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cerrarapp) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void cardData() {

        TextView tv_tit_card = findViewById(R.id.tv_name_card_1);
        tv_tit_card.setText(objCard.getCardAliasName());

        TextView tv_number_card = findViewById(R.id.tv_number_card_1);
        tv_number_card.setText("Nº "+objCard.getCardNumber());

        ImageView img_main_card =  findViewById(R.id.img_main_card_1);
        if(objCard.getMainCard()==1) {
            img_main_card.setVisibility(View.VISIBLE);
        } else {
            img_main_card.setVisibility(View.INVISIBLE);
        }

    }

    public void rechargueOption(){
        System.out.println("entrando al rechargueOption --------------------------->");

        JSONObject postparams  = new JSONObject();
        try {
            postparams.put("transportCardId", objCard.getTransportCardId());
            postparams.put("accountBalance",objCard.getBalance());
        } catch (JSONException e) {
            System.out.println("ERROR PARAMETER:------------------------>"+e.getMessage());
        }

        String tag_json_obj = "json_obj_req";
        String url = Globals.getUrlService()+"/cards/"+CardsAvaiable.get(Globals.getCardSelect()).getTransportCardId()+"/recharge-options?accountBalance="+Globals.getMontoUserMain();

        System.out.println("URL----------------------->"+url);
        System.out.println("param:---------------->"+postparams);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String tmpTxt=response.toString();
                        System.out.println("archivo----------------------------------->"+tmpTxt);

                        try {

                            JSONObject jsonObject=new JSONObject(tmpTxt);
                            JSONArray jsonArray=jsonObject.getJSONArray("result");

                            System.out.println("enfinnnn-------->"+jsonObject.getInt("total"));

                            rechargueOptions=new String[jsonObject.getInt("total")];

                            System.out.println("Elements:-------------------->"+jsonArray.length());

                            if(jsonArray.length() > 0) {

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    System.out.println("option---------->"+jsonArray.get(i).toString());
                                    if(jsonArray.get(i).toString() != null) {
                                        rechargueOptions[i] = jsonArray.get(i).toString();
                                    }
                                }

                                spRechargue.setAdapter(new ArrayAdapter<String>(cardManualChargue.this, android.R.layout.simple_spinner_dropdown_item, rechargueOptions));
                                if(rechargueOptions.length>0){
                                    btSave.setEnabled(true);
                                    tvTextRecMax.setVisibility(View.INVISIBLE);
                                } else {
                                    tvTextRecMax.setVisibility(View.VISIBLE);
                                }
                            }

                            double data = objCard.getRechargeAmountAutomatic();
                            int value = (int)data;
                            spRechargue.setSelection(obtenerPosicionItem(spRechargue, String.valueOf(value)));

                        } catch(JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERROR-------------------->"+error.getMessage());

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("------------------------>"+json);
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                    boolean resultado = json.contains("This card cannot be Recharged, the charge exceeds the card balance limit of 25000");
                    if(resultado){
                        tvTextRecMax.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


    }


    public static int obtenerPosicionItem(Spinner spinner, String fruta) {
        int posicion = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(fruta)) {
                posicion = i;
            }
        }
        return posicion;
    }


    private void saveRechargue() {

        System.out.println("entrando al saveRechargue --------------------------->");

        JSONObject postparams  = new JSONObject();
        try {
            postparams.put("cardNumber", objCard.getCardNumber());
            postparams.put("rechargeAmount", objCard.getRechargeAmount());
        } catch (JSONException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("parameter--------------------->"+postparams.toString());

        String tag_json_obj = "json_obj_req";

        String url = Globals.getUrlService()+"/cards/recharge";

        System.out.println("URL----------------------->"+url);


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("Mensaje salida------------------------>"+json);
                    //Toast.makeText(cardManualChargue.this,  json, Toast.LENGTH_SHORT).show();
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                }
                System.out.println("ERROR VolleyError------------------->"+error.getMessage());
            }
        }){

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                if(mStatusCode==201 || mStatusCode==200){
                    objCard.setBalance(objCard.getBalance()+objCard.getRechargeAmount());
                    Globals.setMontoUserMain(Globals.getMontoUserMain()-objCard.getRechargeAmount());
                    CardsAvaiable.set(Globals.getCardSelect(), objCard);
                    Globals.setCardsAvaiable(CardsAvaiable);
                    setResult(RESULT_OK);
                    finish();
                }
                System.out.println(String.valueOf("ECS------------------->"+mStatusCode));
                return super.parseNetworkResponse(response);
            }
        };
        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


    }

}
