package com.ar.superdigital.gylgroup.transport;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class cardUnlink extends AppCompatActivity {

    ArrayList<ObjCard> CardsAvaiable;
    ObjCard objCard;
    Button bt_desvincular;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_unlink);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        CardsAvaiable=new ArrayList<ObjCard>();
        if(Globals.getCardsAvaiable() != null) {
            CardsAvaiable= Globals.getCardsAvaiable();
        }
        objCard = CardsAvaiable.get(Globals.getCardSelect());

        cardData();

        bt_desvincular=findViewById(R.id.bt_desvincular);
        bt_desvincular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardUnLink();
            }
        });


    }


    private void cardData() {

        TextView tv_tit_card = findViewById(R.id.tv_name_card_1);
        tv_tit_card.setText(objCard.getCardAliasName());

        TextView tv_number_card = findViewById(R.id.tv_number_card_1);
        tv_number_card.setText("Nº "+objCard.getCardNumber());

        ImageView img_main_card =  findViewById(R.id.img_main_card_1);
        if(objCard.getMainCard()==1) {
            img_main_card.setVisibility(View.VISIBLE);
        } else {
            img_main_card.setVisibility(View.INVISIBLE);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cerrarapp) {
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void cardUnLink(){

        System.out.println("entrando al cardUnLink DELETE--------------------------->");

        String tag_json_obj = "json_obj_req";
        String url = Globals.getUrlService()+"/cards/"+objCard.getTransportCardId();
        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.DELETE, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("Mensaje salida------------------------>"+json);
                    Toast.makeText(cardUnlink.this,  json, Toast.LENGTH_SHORT).show();
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                }
                System.out.println("ERROR VolleyError------------------->"+error.getMessage());
            }
        }){

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                if(mStatusCode==201 || mStatusCode==200){
                    CardsAvaiable.remove(Globals.getCardSelect());
                    Globals.setCardsAvaiable(CardsAvaiable);
                    setResult(RESULT_OK);
                    finish();
                }
                System.out.println(String.valueOf("ECS------------------->"+mStatusCode));
                return super.parseNetworkResponse(response);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

}
