package com.ar.superdigital.gylgroup.transport;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class cardUpdate extends AppCompatActivity {

    ArrayList<ObjCard> CardsAvaiable;
    ObjCard objCard;
    EditText nuevoAlias;
    CheckBox main;
    Button guardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_update);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        nuevoAlias=findViewById(R.id.etNuevoAlias);
        main=findViewById(R.id.ch_main);
        guardar=findViewById(R.id.ib_vinculartarjeta);

        CardsAvaiable=new ArrayList<ObjCard>();
        if(Globals.getCardsAvaiable() != null) {
            CardsAvaiable= Globals.getCardsAvaiable();
        }
        objCard = CardsAvaiable.get(Globals.getCardSelect());
        cardData();
        nuevoAlias.setText(objCard.getCardAliasName());

        if(objCard.getMainCard()==1) {
            main.setChecked(true);
            main.setVisibility(View.INVISIBLE);
        }

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ch=0;
                if(main.isChecked()){ ch=1; }
                objCard.setCardAliasName(nuevoAlias.getText().toString());
                objCard.setMainCard(ch);
                System.out.println("ch:-------------------------->"+ch);
                System.out.println("objCard:------------------>"+objCard.getMainCard());
                dataUpdateAcc();
            }
        });

        nuevoAlias.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()>0) {
                    TextView tv_tit_card = findViewById(R.id.tv_name_card_1);
                    tv_tit_card.setText(nuevoAlias.getText().toString());

                }else{
                    System.out.println("no hay texto");
                }
            }
        });


        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isChecked = ((CheckBox)view).isChecked();
                ImageView img_main_card =  findViewById(R.id.img_main_card_1);
                if (isChecked) {
                    img_main_card.setVisibility(View.VISIBLE);
                }
                else {
                    img_main_card.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    private void desMain() {
        for (int i=0; i < CardsAvaiable.size(); i++) {
            if(i != Globals.getCardSelect()) {
                CardsAvaiable.get(i).setMainCard(0);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cerrarapp) {
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void cardData() {

        TextView tv_tit_card = findViewById(R.id.tv_name_card_1);
        tv_tit_card.setText(objCard.getCardAliasName());

        TextView tv_number_card = findViewById(R.id.tv_number_card_1);
        tv_number_card.setText("Nº "+objCard.getCardNumber());

        ImageView img_main_card =  findViewById(R.id.img_main_card_1);
        if(objCard.getMainCard()==1) {
            img_main_card.setVisibility(View.VISIBLE);
        } else {
            img_main_card.setVisibility(View.INVISIBLE);
        }

    }


    private void dataUpdateAcc() {

        System.out.println("entrando al cardAddLink --------------------------->");

        JSONObject postparams  = new JSONObject();

        try {
            postparams.put("transportCardId", objCard.getTransportCardId());
            postparams.put("accountId",Integer.valueOf(Globals.getAccountNumber()));
            postparams.put("cardNumber",objCard.getCardNumber());
            postparams.put("cardTypeId",1);
            postparams.put("cardAliasName", objCard.getCardAliasName());
            postparams.put("mainCard",objCard.getMainCard());
            postparams.put("cardStatusId",1);
        } catch (JSONException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("parameter--------------------->"+postparams.toString());

        String tag_json_obj = "json_obj_req";

        String url = Globals.getUrlService()+"/cards/"+objCard.getTransportCardId();

        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PATCH, url, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("Mensaje salida------------------------>"+json);
                    Toast.makeText(cardUpdate.this,  json, Toast.LENGTH_SHORT).show();
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                }
                System.out.println("ERROR VolleyError------------------->"+error.getMessage());
            }
        }){

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                if(mStatusCode==201 || mStatusCode==200){
                    if(main.isChecked()){ desMain(); }
                    CardsAvaiable.set(Globals.getCardSelect(), objCard);
                    System.out.println("tarjeta:------------>"+Globals.getCardSelect());
                    System.out.println("tarjeta main:---------->"+objCard.getMainCard());
                    Globals.setCardsAvaiable(CardsAvaiable);
                    setResult(RESULT_OK);
                    finish();
                }
                System.out.println(String.valueOf("ECS------------------->"+mStatusCode));
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }






}
