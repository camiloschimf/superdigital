package com.ar.superdigital.gylgroup.transport;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class card_detail extends AppCompatActivity {

    ArrayList<ObjCard> CardsAvaiable;
    ArrayList<objMenuConfig> arrMenuConfig;
    ListView lv_menu;
    adpMenuConfig adpMenuConfig;
    ObjCard objCard;
    TextView montoTarjeta;
    TextView fechaActualizacion;
    int idMenu;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        pDialog = new ProgressDialog(card_detail.this);
        pDialog.setMessage("Espere un Momento");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);

        fechaActualizacion=findViewById(R.id.tv_fechaActualizacion);

        CardsAvaiable=new ArrayList<ObjCard>();

        if(Globals.getCardsAvaiable() != null) {
            CardsAvaiable= Globals.getCardsAvaiable();
        }

        objCard = CardsAvaiable.get(Globals.getCardSelect());
        cardData();

        arrMenuConfig=new ArrayList<objMenuConfig>();
        lv_menu = findViewById(R.id.lv_menuOpciones);

        adpMenuConfig = new adpMenuConfig(card_detail.this, arrMenuConfig);

        viewMenu();

        lv_menu.setAdapter(adpMenuConfig);

        lv_menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                System.out.println("position:------------------------->"+position);
                idMenu=arrMenuConfig.get(position).getId();
                System.out.println("menu:------------------------->"+idMenu);
                invoMenu(idMenu);

            }
        });

        montoTarjeta=findViewById(R.id.tv_montoTarjeta);
        amountData();

    }

    private void invoMenu(int oppMenu){
        switch(oppMenu) {
            case 1:
                Intent intent1 = new Intent(getApplicationContext(), cardManualChargue.class);
                startActivityForResult(intent1, 1);
                break;
            case 2:
                Intent intent2 = new Intent(getApplicationContext(), cardAutomaticCharge.class);
                startActivityForResult(intent2, 2);
                break;
            case 3:
                Intent intent3 = new Intent(getApplicationContext(), cardUpdate.class);
                startActivityForResult(intent3, 3);
                break;
            case 4:
                Intent intent4 = new Intent(getApplicationContext(), cardUnlink.class);
                startActivityForResult(intent4, 4);
                break;
            default:
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        System.out.println("requestCode:--------------------------->"+requestCode);
        System.out.println("resultCode:--------------------------->"+resultCode);
        if ((requestCode == 1) && (resultCode == RESULT_OK)){
            redraw();
        }
        if ((requestCode == 2) && (resultCode == RESULT_OK)){
            redraw();
        }
        if ((requestCode == 3) && (resultCode == RESULT_OK)){
            redraw();
        }
        if ((requestCode == 4) && (resultCode == RESULT_OK)){
            relectura();
        }
    }

    private void redraw(){
        if(Globals.getCardsAvaiable() != null) {
            CardsAvaiable= Globals.getCardsAvaiable();
        }

        objCard = CardsAvaiable.get(Globals.getCardSelect());
        cardData();

        montoTarjeta.setText(String.valueOf(objCard.getBalance()));

        adpMenuConfig = new adpMenuConfig(card_detail.this, arrMenuConfig);
        viewMenu();
        lv_menu.setAdapter(adpMenuConfig);
    }

    private void viewMenu() {

        objMenuConfig mc1= new objMenuConfig(1, "RECARGA MANUAL", null, true, false, false, false, null);
        arrMenuConfig.add(mc1);

        objMenuConfig mc2= new objMenuConfig(2, "RECARGA AUTOMATICA", null, true, false, true, (objCard.getAutomaticRecharge()==1), null);
        arrMenuConfig.add(mc2);

        objMenuConfig mc3= new objMenuConfig(3, "MODIFICAR TARJETA", null, true, false, false, false, null);
        arrMenuConfig.add(mc3);

        objMenuConfig mc4= new objMenuConfig(4, "DESVINCULAR TARJETA", null, true, false, false, false, "#FF0000");
        arrMenuConfig.add(mc4);

    }

    private void cardData() {

        TextView tv_tit_card = findViewById(R.id.tv_name_card_1);
        tv_tit_card.setText(objCard.getCardAliasName());

        TextView tv_number_card = findViewById(R.id.tv_number_card_1);
        tv_number_card.setText("Nº "+objCard.getCardNumber());

        ImageView img_main_card =  findViewById(R.id.img_main_card_1);
        if(objCard.getMainCard()==1) {
            img_main_card.setVisibility(View.VISIBLE);
        } else {
            img_main_card.setVisibility(View.INVISIBLE);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cerrarapp) {
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void amountData() {

        pDialog.show();

        System.out.println("entrando al amountData --------------------------->");

        String tag_json_obj = "json_obj_req";

        String url = Globals.getUrlService()+"/cards/"+objCard.getCardNumber()+"/balance";

        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String tmpTxt=response.toString();
                        System.out.println("archivo----------------------------------->"+tmpTxt);

                        try {

                            JSONObject jsonObject=new JSONObject(tmpTxt);

                            double balance = Double.valueOf(jsonObject.getString("balance"));
                            String strDouble = String.format("%.3f", balance);

                            objCard.setBalance(balance);
                            objCard.setBalanceDate(jsonObject.getString("balanceDate"));

                            CardsAvaiable.get(Globals.getCardSelect()).setBalance(Double.valueOf(jsonObject.getString("balance")));
                            CardsAvaiable.get(Globals.getCardSelect()).setBalanceDate(jsonObject.getString("balanceDate"));

                            montoTarjeta.setText(strDouble);
                            fechaActualizacion.setText("ULTIMA ACTUALIZACION "+jsonObject.getString("balanceDate"));


                            System.out.println("balance ---------------------------->" + jsonObject.getString("balance"));

                            Globals.setCardsAvaiable(CardsAvaiable);

                        } catch(JSONException e) {
                            System.out.println("JSONException:--------------------->"+e.getMessage());
                            e.printStackTrace();
                        }

                        pDialog.dismiss();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERROR-------------------->"+error.getMessage());

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("------------------------>"+json);
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                }
            }
        });

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    private void relectura() {
        System.out.println("entrando al cardsAvailable --------------------------->");

        String tag_json_obj = "json_obj_req";
        String url = Globals.getUrlService()+"/accounts/"+Globals.getAccountNumber()+"/linked-cards";

        System.out.println("URL----------------------->"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String tmpTxt=response.toString();
                        System.out.println("archivo----------------------------------->"+tmpTxt);

                        try {

                            JSONObject jsonObject=new JSONObject(tmpTxt);
                            JSONArray jsonArray=jsonObject.getJSONArray("result");

                            System.out.println("Elements:-------------------->"+jsonArray.length());

                            if(jsonArray.length() > 0) {

                                CardsAvaiable=new ArrayList<ObjCard>();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    objCard = new ObjCard();
                                    objCard.setTransportCardId(Long.parseLong(jsonObject1.getString("transportCardId")));
                                    objCard.setAccountId(Long.parseLong(jsonObject1.getString("accountId")));
                                    objCard.setCardNumber(Long.parseLong(jsonObject1.getString("cardNumber")));
                                    objCard.setCardTypeId(Integer.parseInt(jsonObject1.getString("cardTypeId")));
                                    objCard.setCardStatusId(Integer.parseInt(jsonObject1.getString("cardStatusId")));
                                    objCard.setCardAliasName(jsonObject1.getString("cardAliasName"));
                                    objCard.setAutomaticRecharge(Integer.parseInt(jsonObject1.getString("automaticRecharge")));
                                    objCard.setMainCard(Integer.parseInt(jsonObject1.getString("mainCard")));
                                    objCard.setTypeDescription(jsonObject1.getString("typeDescription"));
                                    objCard.setStatusDescription(jsonObject1.getString("statusDescription"));
                                    objCard.setMinBalanceRecharge(Double.parseDouble(jsonObject1.getString("minBalanceRecharge")));
                                    objCard.setRechargeAmount(Double.parseDouble(jsonObject1.getString("rechargeAmount")));
                                    objCard.setRechargeAmountAutomatic(Double.parseDouble(jsonObject1.getString("rechargeAmount")));
                                    objCard.setHeight(Globals.getColapsedCardHeight());

                                    CardsAvaiable.add(objCard);

                                    System.out.println("Numero de tarjeta ---------------------------->" + jsonObject1.getString("cardNumber"));

                                }
                                CardsAvaiable.get(CardsAvaiable.size() - 1).setHeight(Globals.getCardHeight());
                                Globals.setCardsAvaiable(CardsAvaiable);

                            }

                            setResult(RESULT_OK);
                            finish();

                        } catch(JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERROR-------------------->"+error.getMessage());

                String json = null;
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    json = new String(networkResponse.data);
                    System.out.println("------------------------>"+json);
                    System.out.println("StatusCode------------------>"+networkResponse.statusCode);
                    if(networkResponse.statusCode==500) {
                        relectura();
                    }
                }
            }
        });

        jsonObjReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

}
