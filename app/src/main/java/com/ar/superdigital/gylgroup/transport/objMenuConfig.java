package com.ar.superdigital.gylgroup.transport;

public class objMenuConfig {

    private int Id;
    private String titulo;
    private String subtitulo;
    private boolean addAbierto;
    private boolean abierto;
    private boolean addActivo;
    private boolean activo;
    private String titleColor;

    public objMenuConfig() {
    }

    public objMenuConfig(int id, String titulo, String subtitulo, boolean addAbierto, boolean abierto, boolean addActivo, boolean activo, String titleColor) {
        Id = id;
        this.titulo = titulo;
        this.subtitulo = subtitulo;
        this.addAbierto = addAbierto;
        this.abierto = abierto;
        this.addActivo = addActivo;
        this.activo = activo;
        this.titleColor = titleColor;
    }



    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }

    public boolean isAddAbierto() {
        return addAbierto;
    }

    public void setAddAbierto(boolean addAbierto) {
        this.addAbierto = addAbierto;
    }

    public boolean isAbierto() {
        return abierto;
    }

    public void setAbierto(boolean abierto) {
        this.abierto = abierto;
    }

    public boolean isAddActivo() {
        return addActivo;
    }

    public void setAddActivo(boolean addActivo) {
        this.addActivo = addActivo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getTitleColor() {
        return titleColor;
    }

    public void setTitleColor(String titleColor) {
        this.titleColor = titleColor;
    }
}
